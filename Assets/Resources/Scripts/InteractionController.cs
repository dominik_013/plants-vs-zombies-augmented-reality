﻿using Resources.Scripts.plants;
using UnityEngine;

namespace Resources.Scripts
{
    public class InteractionController : MonoBehaviour, IToolChangedMessageTarget, IPlantChangedMessageTarget
    {
        private ScoreController _scoreController;
        private GameObject _currentSelection;
    
        private string _activeTool;
        private string _activePlant;
        private GameObject _activePlantPrefab;
    
        void Start()
        {
            _scoreController = GetComponent<ScoreController>();
            _currentSelection = null;
        }

        void Update()
        {
            // Check if a GameObject is still selected and apply tool function
            if (_currentSelection != null)
            {
                if (Input.touchCount > 0)
                {
                    var basePlant = _activePlantPrefab.GetComponent<BasePlant>();
                    var spawnPlane = _currentSelection.GetComponent<SpawnPlane>();
                
                    switch (_activeTool)
                    {
                        case Toolbelt.Tool.Hand:
                            var resource = _currentSelection.GetComponent<Resource>();
                            resource.PickupResource();
                            _scoreController.AddScore(resource.Worth);
                            break;
                        case Toolbelt.Tool.Remove:
                            if (spawnPlane.IsOccupied)
                            {
                                var plant = spawnPlane.GetGameObject();
                                _scoreController.AddScore(basePlant.Price / 2);
                                Destroy(plant);
                                spawnPlane.IsOccupied = false;
                            }

                            break;
                        case Toolbelt.Tool.Plant:                       
                            if (!spawnPlane.IsOccupied && _scoreController.CanBuyPlant(basePlant.Price))
                            {
                                _scoreController.BuyPlant(basePlant.Price);
                                var go = Instantiate(_activePlantPrefab, _currentSelection.transform);
                                spawnPlane.SetGameObject(go);
                                spawnPlane.IsOccupied = true;
                            }
                        
                            break;
                    }
                }
            
                // Delete outline again
                var outline = _currentSelection.GetComponent<Outline>();
                if (outline != null)
                {
                    outline.enabled = false;
                }
            }
        
            // Bit shift the index of the layer (15) to get a bit mask
            int layerMask = 0;

            // Set the according layermask to only highlight things that can be either selected / planted / removed
            switch (_activeTool)
            {
                case Toolbelt.Tool.Hand:
                    layerMask = 1 << 15;
                    break;
                case Toolbelt.Tool.Remove:
                case Toolbelt.Tool.Plant:
                    layerMask = 1 << 16;
                    break;
            }
        
            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
            {
                _currentSelection = hit.collider.transform.gameObject;
                var outline = _currentSelection.GetComponent<Outline>();
                if (outline != null)
                {
                    outline.enabled = true;
                }
            }
            else
            {
                _currentSelection = null;
            }
        }

        public void ToolChanged(string newTool)
        {
            _activeTool = newTool;
        }

        public void PlantChanged(string newPlant, GameObject prefab)
        {
            _activePlant = newPlant;
            _activePlantPrefab = prefab;
        }
    }
}