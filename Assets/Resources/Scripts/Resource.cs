﻿using System.Collections;
using UnityEngine;

public class Resource : MonoBehaviour
{
    [Range(6f, 25f)] public float TimeToDespawn = 5f;
    [Range(0.01f, 0.05f)] public float WaveHeight = 0.1f;
    [Range(.5f, 20f)] public float Frequency = 5f;
    [Range(0.5f, 5f)] public float RotationSpeedY = 1.5f;

    public int Worth = 25;

    private float _timeSpawned;
    private Vector3 _initialPosition;

    private Renderer _renderer;
    private float _blinkTime = 3f;
    private bool _blinkStarted;

    void Start()
    {
        _timeSpawned = Time.time;
        _initialPosition = transform.position;
        _renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        if (_timeSpawned + TimeToDespawn - _blinkTime < Time.time && !_blinkStarted)
        {
            _blinkStarted = true;
            StartCoroutine(RendererBlink(_blinkTime));
        }
        if (_timeSpawned + TimeToDespawn < Time.time)
        {
            Destroy(gameObject);
        }

        var offsetY = new Vector3(0f, Mathf.Sin(Time.time * Frequency) * WaveHeight, 0f);

        transform.position = _initialPosition + offsetY;
        transform.Rotate(Vector3.up, RotationSpeedY);
    }

    public void PickupResource()
    {
        Destroy(gameObject);
    }

    private IEnumerator RendererBlink(float duration)
    {
        var starTime = Time.time;

        while (starTime + duration > Time.time)
        {
            _renderer.material.color = new Color(
                _renderer.material.color.r,
                _renderer.material.color.g,
                _renderer.material.color.b, 0f);
            yield return new WaitForSeconds(0.5f);
            _renderer.material.color = new Color(
                _renderer.material.color.r,
                _renderer.material.color.g, 
                _renderer.material.color.b, 255f);
        }
    }
}