﻿using Resources.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;
using Vuforia;

public class GameStateController : MonoBehaviour, ITrackableEventHandler
{
    public GameObject ResourceSpawnPlane;

    private void Start()
    {
        GetComponent<TrackableBehaviour>().RegisterTrackableEventHandler(this);
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            ExecuteEvents.Execute<IGameStateChangedMessageTarget>(ResourceSpawnPlane, null,
                (handler, data) => handler.GameStarted());
        }
        else
        {
            ExecuteEvents.Execute<IGameStateChangedMessageTarget>(ResourceSpawnPlane, null,
                (handler, data) => handler.GameFinished());
        }
    }
}
