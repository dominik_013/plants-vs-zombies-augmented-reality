﻿using UnityEngine;
using UnityEngine.UI;

namespace Resources.Scripts
{
    public class ScoreController : MonoBehaviour
    {
        public Text ScoreText;
        
        private int _currentScore;

        private void Start()
        {
            _currentScore = 200;
            UpdateScoreText();
        }

        public void AddScore(int amount)
        {
            _currentScore += amount;
            UpdateScoreText();
        }

        public bool CanBuyPlant(int cost)
        {
            return _currentScore - cost >= 0;
        }

        public void BuyPlant(int cost)
        {
            _currentScore -= cost;
            UpdateScoreText();
        }

        private void UpdateScoreText()
        {
            ScoreText.text = string.Format("Score: {0}", _currentScore.ToString("D5"));
        }
    }
}