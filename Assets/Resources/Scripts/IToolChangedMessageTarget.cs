﻿using UnityEngine.EventSystems;

namespace Resources.Scripts
{
    public interface IToolChangedMessageTarget : IEventSystemHandler
    {
        void ToolChanged(string newTool);
    }
}
