﻿using System;
using Resources.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;
using Vuforia;

public class Toolbelt : MonoBehaviour, IVirtualButtonEventHandler
{
    private const string DEFAULT_TOOL = Tool.Hand;
    public UnityEngine.UI.Image CurrentSelection;
    public GameObject ToolAction;

    private ResourceRequest _handRequest;
    private ResourceRequest _removeRequest;
    private ResourceRequest _plantRequest;
    
    public static class Tool
    {
        public const string Hand = "HandTool";
        public const string Remove = "RemoveTool";
        public const string Plant = "PlantTool";
    }
    
    // Start is called before the first frame update
    void Start()
    {
        var buttonHand = GameObject.Find(Tool.Hand);
        var buttonRemove = GameObject.Find(Tool.Remove);
        var buttonPlant = GameObject.Find(Tool.Plant);
        
        buttonHand.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        buttonRemove.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        buttonPlant.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        
        
        _handRequest = UnityEngine.Resources.LoadAsync<Sprite>("Images/tools/tools_hand/hand_crop");
        _removeRequest = UnityEngine.Resources.LoadAsync<Sprite>("Images/tools/tools_remove/remove_crop");
        _plantRequest = UnityEngine.Resources.LoadAsync<Sprite>("Images/tools/tools_plant/plant_crop");
        
        ExecuteEvents.Execute<IToolChangedMessageTarget>(ToolAction, null,
            (handler, data) => handler.ToolChanged(DEFAULT_TOOL));
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        // Change tool-sprite in UI
        switch (vb.VirtualButtonName)
        {
                case Tool.Hand:                   
                    if (_handRequest.isDone)
                    {
                        CurrentSelection.sprite = (Sprite) _handRequest.asset;
                    }
                        
                    break;
                    
                case Tool.Remove:
                    if (_removeRequest.isDone)
                    {
                        CurrentSelection.sprite = (Sprite) _removeRequest.asset;
                    }
                    break;
                        
                case Tool.Plant:
                    if (_plantRequest.isDone)
                    {
                        CurrentSelection.sprite = (Sprite) _plantRequest.asset;
                    }
                    break;
        }
        
        // Update active tool in ToolAction
        ExecuteEvents.Execute<IToolChangedMessageTarget>(ToolAction, null,
            (handler, data) => handler.ToolChanged(vb.VirtualButtonName));
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Debug.Log("Released " + vb.VirtualButtonName);
    }
}
