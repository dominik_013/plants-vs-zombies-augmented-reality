﻿using UnityEngine.EventSystems;

namespace Resources.Scripts
{
    public interface IGameStateChangedMessageTarget : IEventSystemHandler
    {
        void GameStarted();
        void GameFinished();
    }
}