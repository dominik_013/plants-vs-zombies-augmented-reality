﻿using Resources.Scripts.plants;
using UnityEngine;

namespace Resources.Scripts
{
    public class Zombie : MonoBehaviour
    {
        public float Health = 100f;
        public float RunSpeed = .3f;
        public int DamageAmount = 50;
        public float MinimumAttackDuration = 2f;
        private float _inAttackRangeStartTime;

        private bool _canRun = true;

        private bool _triggeredPlant;
        private Collider _colliderPlant;

        private bool _triggeredZombie;
        private Collider _colliderZombie;

        // Update is called once per frame
        void Update()
        {
            if (Health <= 0)
            {
                Destroy(gameObject);
            }
            
            if (_canRun)
            {
                transform.Translate(Vector3.left * RunSpeed * Time.deltaTime);
            }
            
            if (_triggeredPlant && !_colliderPlant)
            {
                _triggeredPlant = false;
                _canRun = true;
            }
            
            if (_triggeredZombie && !_colliderZombie)
            {
                _triggeredZombie = false;
                _canRun = true;
            }
        }
    
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(GameConstant.Layer.Projectile))
            {
                var projectile = other.gameObject.GetComponent<Projectile>();
                Health -= projectile.Damage;
                Destroy(other.transform.gameObject);
            }
            
            if (other.gameObject.layer == LayerMask.NameToLayer(GameConstant.Layer.Plant))
            {
                _triggeredPlant = true;
                _colliderPlant = other;
                _canRun = false;
                _inAttackRangeStartTime = Time.time;
            }

            // When running into another zombie
            if (other.gameObject.layer == LayerMask.NameToLayer(GameConstant.Layer.Zombie))
            {
                _triggeredZombie = true;
                _colliderZombie = other;
                _canRun = false;
            }

        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(GameConstant.Layer.Plant))
            {
                var elapsedTime = Time.time - _inAttackRangeStartTime;
                if (elapsedTime > MinimumAttackDuration)
                {
                    var plant = other.gameObject.GetComponent<BasePlant>();
                    plant.Damage(DamageAmount);
                    _inAttackRangeStartTime = Time.time;
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(GameConstant.Layer.Zombie))
            {
                _canRun = true;
            }
        }
    }
}
