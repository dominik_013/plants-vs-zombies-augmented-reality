﻿using UnityEngine;

namespace Resources.Scripts
{
    public class Projectile : MonoBehaviour
    {
        public int Damage = 20;
        public float ShootSpeed = 1f;

        void Update()
        {
            transform.Translate(Vector3.left * ShootSpeed * Time.deltaTime);
            
        }
    }
}
