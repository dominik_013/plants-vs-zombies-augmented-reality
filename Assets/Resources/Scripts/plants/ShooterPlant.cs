﻿using System.Collections.Generic;
using UnityEngine;

namespace Resources.Scripts.plants
{
    public class ShooterPlant : BasePlant
    {
        public int ShootDelay = 3;
        public GameObject Projectile;
        
        // Start is called before the first frame update
        void Start()
        {
            InvokeRepeating(nameof(ShootProjectile), 0, ShootDelay);
        }

        public void ShootProjectile()
        {
            Instantiate(Projectile, transform);
        }
    }
}
