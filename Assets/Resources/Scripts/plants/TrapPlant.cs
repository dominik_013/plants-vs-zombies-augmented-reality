﻿using UnityEngine;

namespace Resources.Scripts.plants
{
    public class TrapPlant : BasePlant
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(GameConstant.Layer.Zombie))
            {
                // First destroy the zombie and then destroy trap plant
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
