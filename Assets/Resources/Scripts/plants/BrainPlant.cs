﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Resources.Scripts.plants
{
    public class BrainPlant : BasePlant
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(GameConstant.Layer.Zombie))
            {
                SceneManager.LoadScene(GameConstant.Scene.GameScene);
            }
        }
    }
}
