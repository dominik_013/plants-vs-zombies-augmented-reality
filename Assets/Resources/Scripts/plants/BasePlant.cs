﻿using UnityEngine;

namespace Resources.Scripts.plants
{
    public class BasePlant : MonoBehaviour
    {
        public int Health = 100;
        public int Price = 50;

        private float _inAttackRangeStartTime;

        private void Update()
        {
            if (Health <= 0)
            {
                Destroy(gameObject);
            }
        }

        public void Damage(int amount)
        {
            Health -= amount;
        }
    }
}
