﻿using UnityEngine;

namespace Resources.Scripts
{
    public class ZombieSpawner : MonoBehaviour
    {
        public GameObject Zombie;
        public float RepeatRateMin = 10f;
        public float RepeatRateMax = 15f;

        private int _spawnerCount;
    
        // Start is called before the first frame update
        void Start()
        {
            _spawnerCount = transform.childCount;
            Invoke(nameof(SpawnZombie), Random.Range(RepeatRateMin, RepeatRateMax));
        }

        void SpawnZombie()
        {
            var tr = transform.GetChild(Random.Range(0, _spawnerCount - 1));
            Instantiate(Zombie, tr);
            Invoke(nameof(SpawnZombie), Random.Range(RepeatRateMin, RepeatRateMax));
        }
    }
}
