﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Vuforia;

namespace Resources.Scripts
{
    public class Plants : MonoBehaviour, IVirtualButtonEventHandler
    {
        private const string DEFAULT_PLANT = Plant.Shooter;
        public UnityEngine.UI.Image CurrentSelection;
        public GameObject ToolAction;

        public GameObject ShooterPrefab;
        public GameObject WallPrefab;
        public GameObject TrapPrefab;
    
        private ResourceRequest _shooterRequest;
        private ResourceRequest _wallRequest;
        private ResourceRequest _trapRequest;
        
        public static class Plant
        {
            public const string Shooter = "ShooterPlant";
            public const string Wall = "WallPlant";
            public const string Trap = "TrapPlant";
        }
        
        // Start is called before the first frame update
        void Start()
        {
            var buttonShooter = GameObject.Find(Plant.Shooter);
            var buttonWall = GameObject.Find(Plant.Wall);
            var buttonTrap = GameObject.Find(Plant.Trap);
            
            buttonShooter.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
            buttonWall.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
            buttonTrap.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
            
            
            _shooterRequest = UnityEngine.Resources.LoadAsync<Sprite>("Images/plants/plants_shooter_crop");
            _wallRequest = UnityEngine.Resources.LoadAsync<Sprite>("Images/plants/plants_wall_crop");
            _trapRequest = UnityEngine.Resources.LoadAsync<Sprite>("Images/plants/plants_trap_crop");
            
            ExecuteEvents.Execute<IPlantChangedMessageTarget>(ToolAction, null,
                (handler, data) => handler.PlantChanged(DEFAULT_PLANT, GetPrefab(DEFAULT_PLANT)));
        }
    
        public void OnButtonPressed(VirtualButtonBehaviour vb)
        {
            // Change tool-sprite in UI
            switch (vb.VirtualButtonName)
            {
                    case Plant.Shooter:                   
                        if (_shooterRequest.isDone)
                        {
                            CurrentSelection.sprite = (Sprite) _shooterRequest.asset;
                        }
                            
                        break;
                        
                    case Plant.Wall:
                        if (_wallRequest.isDone)
                        {
                            CurrentSelection.sprite = (Sprite) _wallRequest.asset;
                        }
                        break;
                            
                    case Plant.Trap:
                        if (_trapRequest.isDone)
                        {
                            CurrentSelection.sprite = (Sprite) _trapRequest.asset;
                        }
                        break;
            }
            
            // Update active plant in ToolAction
            ExecuteEvents.Execute<IPlantChangedMessageTarget>(ToolAction, null,
                (handler, data) => handler.PlantChanged(vb.VirtualButtonName, GetPrefab(vb.VirtualButtonName)));
        }
    
        public void OnButtonReleased(VirtualButtonBehaviour vb)
        {
            Debug.Log("Released " + vb.VirtualButtonName);
        }
        
        private GameObject GetPrefab(string plant)
        {
            switch (plant)
            {
                case Plant.Shooter:
                    return ShooterPrefab;
                case Plant.Wall:
                    return WallPrefab;
                case Plant.Trap:
                    return TrapPrefab;
            }

            return null;
        }
    }
}