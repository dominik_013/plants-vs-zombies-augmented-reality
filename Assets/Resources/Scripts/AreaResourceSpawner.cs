﻿using UnityEngine;

namespace Resources.Scripts
{
    public class AreaResourceSpawner : MonoBehaviour, IGameStateChangedMessageTarget
    {
        public GameObject ResourceToSpawn;
    
        public float Padding = .2f;
        public float MinSpawnInterval = 5f;
        public float MaxSpawnInterval = 15f;

        private float _minX;
        private float _maxX;
        private float _minZ;
        private float _maxZ;

        private bool _isGameStarted;
        private float _time;
        private float _nextSpawnInterval;
        private MeshRenderer _meshRenderer;
    
        void Start()
        {
            _nextSpawnInterval = Random.Range(MinSpawnInterval, MaxSpawnInterval);
            _meshRenderer = GetComponent<MeshRenderer>();
        }

        void Update()
        {
            if (ResourceToSpawn != null && _isGameStarted)
            {
                _time += Time.deltaTime;

                if (_time > _nextSpawnInterval)
                {
                    var bounds = _meshRenderer.bounds.size;
                    var center = transform.position;

                    _minX = center.x - bounds.x / 2 + Padding;
                    _maxX = center.x + bounds.x / 2 - Padding;
                    _minZ = center.z - bounds.z / 2 + Padding;
                    _maxZ = center.z + bounds.z / 2 - Padding;
                
                    var randX = Random.Range(_minX, _maxX);
                    var randZ = Random.Range(_minZ, _maxZ);
                    Instantiate(ResourceToSpawn, new Vector3(randX, transform.position.y + 0.1f, randZ), Quaternion.identity);
                    _nextSpawnInterval = Random.Range(MinSpawnInterval, MaxSpawnInterval);
                    _time = 0;
                }
            }
        }

        public void GameStarted()
        {
            _isGameStarted = true;
        }

        public void GameFinished()
        {
            _isGameStarted = false;
            _time = 0;
        }
    }
}
