﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Resources.Scripts
{
    public class IngameMenuController : MonoBehaviour
    {
        public GameObject IngameMenu;
        public GameObject SelectionCircle;

        private float _defaultTimeScale;

        private void Start()
        {
            _defaultTimeScale = Time.timeScale;
        }

        public void OpenIngameMenu()
        {
            IngameMenu.SetActive(true);
            SelectionCircle.SetActive(false);
            Time.timeScale = 0;
        }

        public void CloseIngameMenu()
        {
            IngameMenu.SetActive(false);
            SelectionCircle.SetActive(true);
            Time.timeScale = _defaultTimeScale;
        }

        public void GoToMainMenu()
        {
            SceneManager.LoadScene(GameConstant.Scene.MenuScene);
        }
    }
}