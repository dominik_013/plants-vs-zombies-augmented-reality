﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Resources.Scripts
{
    public class MainMenuController : MonoBehaviour
    {

        public void PlayGame()
        {
            SceneManager.LoadScene(GameConstant.Scene.GameScene);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}