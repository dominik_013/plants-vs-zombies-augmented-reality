﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Resources.Scripts
{
    public interface IPlantChangedMessageTarget : IEventSystemHandler
    {
        void PlantChanged(string newPlant, GameObject prefab);
    }
}