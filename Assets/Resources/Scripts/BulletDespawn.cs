﻿using UnityEngine;

namespace Resources.Scripts
{
    public class BulletDespawn : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Projectile"))
            {
                Destroy(other.gameObject);
            }
        }
    }
}
