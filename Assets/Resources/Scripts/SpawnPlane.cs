﻿using UnityEngine;

public class SpawnPlane : MonoBehaviour
{
    private GameObject _obj;
    
    public bool IsOccupied { get; set; }

    public void SetGameObject(GameObject obj)
    {
        _obj = obj;
    }

    public GameObject GetGameObject()
    {
        return _obj;
    }
}
