﻿namespace Resources.Scripts
{
    public class GameConstant
    {
        public class Layer
        {
            public static string Projectile = "Projectile";
            public static string Plant = "Plant";
            public static string Zombie = "Zombie";
        }

        public class Scene
        {
            public static string GameScene = "GameScene";
            public static string MenuScene = "MenuScene";
        }
    }
}